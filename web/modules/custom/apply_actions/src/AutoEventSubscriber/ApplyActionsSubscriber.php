<?php

namespace Drupal\apply_actions\AutoEventSubscriber;

use Wamania\Snowball\StemmerManager;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\core_event_dispatcher\Event\Entity\EntityPresaveEvent;
use Drupal\hook_event_dispatcher\HookEventDispatcherInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Apply actions event subscriber.
 */
class ApplyActionsSubscriber implements EventSubscriberInterface {

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Constructs event subscriber.
   *
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger.
   */
  public function __construct(MessengerInterface $messenger) {
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      HookEventDispatcherInterface::ENTITY_PRE_SAVE => ['onEntityPresave'],
    ];
  }

  /**
   * Handler for presaving an entity.
   *
   * @param \Drupal\core_event_dispatcher\Event\Entity\EntityPresaveEvent $event
   *   Entity event.
   */
  public function onEntityPresave(EntityPresaveEvent $event) {
    $entity = $event->getEntity();
    if ($entity->getEntityTypeId() === 'node' && $entity->getType() === 'definition') {
      $word = $entity->label();

      $stemmer = new StemmerManager();
      $wordStem = $stemmer->stem($word, $entity->language()->getId());
      $entityTypeManager = \Drupal::entityTypeManager();
      $nodeStorage = $entityTypeManager->getStorage('node');
      $query = $nodeStorage->getQuery();
      $query->condition('type', 'entry');
      $query->condition('stem', $wordStem);
      $entryIds = $query->execute();
      if (empty($entryIds)) {
        $entry = $nodeStorage->create([
          'title' => $word,
          'stem' => $wordStem,
          'type' => 'entry',
          'status' => $entity->isPublished(),
        ]);
        $entry->save();
        $entryId = $entry->id();
      }
      else {
        $entryId = reset($entryIds);
        $original = $event->getOriginalEntity();
        // When the definition gets published, publish the entry.
        if ($original && ($entity->isPublished() && !$original->isPublished())) {
          $entry = $nodeStorage->load($entryId);
          $entry->setPublished(TRUE);
          $entry->save();
        }
      }
      $entity->entry->target_id = $entryId;
    }
  }

}
