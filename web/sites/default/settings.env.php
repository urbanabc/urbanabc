<?php
$settings['config_sync_directory'] = '../config/sync';

$settings['file_public_path'] = 'files';
$settings['file_private_path'] = '../../private';
$settings['trusted_host_patterns'] = array_filter(explode(',', getenv('VIRTUAL_HOST')), function($host){return '^' . addcslashes($host, '.') . '$';});

$config['system.site']['mail'] = getenv('SITE_EMAIL');
$config['update.settings']['notification']['emails'] = explode(',', getenv('UPDATE_NOTIFICATION_EMAILS'));

if (getenv('HASH_SALT')) {
  $settings['hash_salt'] = getenv('HASH_SALT');
}

if (getenv('MAIL_SMTPMODE') === 'smtp') {
  $config['mailsystem.settings']['defaults'] = [
    'sender' => 'swiftmailer',
    'formatter' => 'swiftmailer',
  ];
  $config['swiftmailer.transport']['smtp_credential_provider'] = 'swiftmailer';
  $config['swiftmailer.transport']['transport'] = getenv('MAIL_SMTPMODE');
  $config['swiftmailer.transport']['smtp_host'] = getenv('MAIL_SMTPHOST');
  $config['swiftmailer.transport']['smtp_port'] = getenv('MAIL_SMTPPORT');
  $config['swiftmailer.transport']['smtp_encryption'] = getenv('MAIL_SMTPSECURE');
  $config['swiftmailer.transport']['smtp_credentials'] = [
    'swiftmailer' => [
      'username' => getenv('MAIL_SMTPNAME'),
      'password' => getenv('MAIL_SMTPPASSWORD'),
    ],
  ];
}

if (!\Drupal\Core\Installer\InstallerKernel::installationAttempted() && class_exists('Drupal\redis\Cache\PhpRedis')) {
  $settings['redis.connection']['interface'] = 'PhpRedis';
  $settings['redis.connection']['host'] = 'redis';
  $settings['cache']['default'] = 'cache.backend.redis';
}

/**
 * Remove 'field_prefix' for new fields.
 */
$config['field_ui.settings']['field_prefix'] = '';

$databases['default']['default'] = [
  'database' => getenv('MYSQL_DATABASE'),
  'username' => getenv('MYSQL_USER'),
  'password' => getenv('MYSQL_PASSWORD'),
  'host'     => getenv('MYSQL_HOST'),
  'port' => '3306',
  'driver' => 'mysql',
  'prefix' => '',
  'collation' => 'utf8mb4_general_ci',
];

if (getenv('DEVEL_ENABLED')) {
  $settings['skip_permissions_hardening'] = TRUE;
  $settings['class_loader_auto_detect'] = FALSE;
  /**
   * Assertions.
   *
   * The Drupal project primarily uses runtime assertions to enforce the
   * expectations of the API by failing when incorrect calls are made by code
   * under development.
   *
   * @see http://php.net/assert
   * @see https://www.drupal.org/node/2492225
   *
   * If you are using PHP 7.0 it is strongly recommended that you set
   * zend.assertions=1 in the PHP.ini file (It cannot be changed from .htaccess
   * or runtime) on development machines and to 0 in production.
   *
   * @see https://wiki.php.net/rfc/expectations
   */
  assert_options(ASSERT_ACTIVE, TRUE);
  \Drupal\Component\Assertion\Handle::register();

  /**
   * Show all error messages, with backtrace information.
   *
   * In case the error level could not be fetched from the database, as for
   * example the database connection failed, we rely only on this value.
   */
  $config['system.logging']['error_level'] = 'verbose';

  /**
   * Disable CSS and JS aggregation.
   */
  $config['system.performance']['css']['preprocess'] = FALSE;
  $config['system.performance']['js']['preprocess'] = FALSE;

  /**
   * Enable 'devel_config' in 'config_split' module.
   */
  $config['config_split.config_split.dev']['status'] = TRUE;
  $config['contact.settings']['flood']['limit'] = 500;

  /**
   * Enable local development services and null cache bins.
   */
  if (!\Drupal\Core\Installer\InstallerKernel::installationAttempted()) {
    $settings['container_yamls'][] = DRUPAL_ROOT . '/sites/custom.development.services.yml';

    /**
     * Disable the render cache (this includes the page cache).
     *
     * Note: you should test with the render cache enabled, to ensure the correct
     * cacheability metadata is present. However, in the early stages of
     * development, you may want to disable it.
     *
     * This setting disables the render cache by using the Null cache back-end
     * defined by the development.services.yml file above.
     *
     * Do not use this setting until after the site is installed.
     */
    $settings['cache']['bins']['render'] = 'cache.backend.null';

    /**
     * Disable Internal Page Cache.
     *
     * Note: you should test with Internal Page Cache enabled, to ensure the correct
     * cacheability metadata is present. However, in the early stages of
     * development, you may want to disable it.
     *
     * This setting disables the page cache by using the Null cache back-end
     * defined by the development.services.yml file above.
     *
     * Only use this setting once the site has been installed.
     */
    $settings['cache']['bins']['page'] = 'cache.backend.null';

    /**
     * Disable Dynamic Page Cache.
     *
     * Note: you should test with Dynamic Page Cache enabled, to ensure the correct
     * cacheability metadata is present (and hence the expected behavior). However,
     * in the early stages of development, you may want to disable it.
     */
    $settings['cache']['bins']['dynamic_page_cache'] = 'cache.backend.null';
    /**
     * Disable caching for migrations.
     *
     * Uncomment the code below to only store migrations in memory and not in the
     * database. This makes it easier to develop custom migrations.
     */
    $settings['cache']['bins']['discovery_migration'] = 'cache.backend.memory';
  }
}
